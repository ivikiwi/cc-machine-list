import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import { MachineRestService } from './machine-rest.service';
import { take } from 'rxjs';

describe('MachineRestService', () => {
  let service: MachineRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule], 
      providers: [MachineRestService]
    });
    service = TestBed.inject(MachineRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  
  it('should return machines', (
    //done
  ) => {
    service.getMachines$().subscribe(machines => {
      expect(machines.data.length).toBeGreaterThan(0);
      //done();
    });
  });
});
