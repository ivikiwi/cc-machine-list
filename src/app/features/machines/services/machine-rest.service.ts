import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MachinesData } from '../interfaces/machines-data';
import { Observable } from 'rxjs';
import { MachineData } from '../interfaces/machine-data';

@Injectable({
  providedIn: 'root'
})
export class MachineRestService {
  private apiUrl: string = 'https://machinestream.herokuapp.com';

  constructor(private http: HttpClient) { }

  public getMachines$(): Observable<MachinesData> {
    return this.http.get<MachinesData>(this.apiUrl + '/api/v1/machines');
  }

  public getMachineItem$(machineId: string): Observable<MachineData> {
    return this.http.get<MachineData>(this.apiUrl +  `/api/v1/machines/${machineId}`);
  }  
}
