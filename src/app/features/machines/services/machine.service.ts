import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MachineStatusColor } from '../enums/machine-status-color';
import { Machine } from '../interfaces/machine';
import { MachineEvent } from '../interfaces/machine-event';
import { MachineStatus } from '../types/machine-status-type';
import { MachineRestService } from './machine-rest.service';
import * as phoenix from 'phoenix';
import { Socket } from 'phoenix';
import { MachineUpdateStatusData } from '../interfaces/machine-update-status-data';

@Injectable({
  providedIn: 'root'
})
export class MachineService {
  public machines$: Observable<Machine[]>;
  public initialMachines$: Observable<Machine[]>;

  public updatedMachinesSubject = new BehaviorSubject<MachineUpdateStatusData[]>([]);
  public updatedMachines$ = this.updatedMachinesSubject.asObservable();

  constructor(private machineRestService: MachineRestService) {
    this.defineStreams();
    this.initSocket();
  }

  public getLastMachineEvents$(machineId: string): Observable<MachineEvent[] | undefined> {
    return this.machineRestService.getMachineItem$(machineId).pipe(
      map(machineItem => machineItem.data.events)
    );
  }

  private initSocket(): void {
    const socket = new Socket('//machinestream.herokuapp.com/api/v1/events');
    socket.connect();
    const channel = socket.channel("events", {});
    channel.join();
    channel.on("new", (machineUpdateStatusData: MachineUpdateStatusData) => {
      // Check, if machine id of new emitted data is already in updatedMachinesSubject
      // if true: update - if false: push new data to updatedMachinesSubject
      const updatedMachines = this.updatedMachinesSubject.getValue();
      const isMachineAlreadyUpdated = updatedMachines.some(updatedMachine => updatedMachine.machine_id === machineUpdateStatusData.machine_id);
      if(!isMachineAlreadyUpdated) {
        this.updatedMachinesSubject.next([...updatedMachines, machineUpdateStatusData]);
        return;
      }
      const newUpdatedMachines = updatedMachines.map(updatedMachine => updatedMachine.machine_id === machineUpdateStatusData.machine_id ? machineUpdateStatusData : updatedMachine);
      this.updatedMachinesSubject.next(newUpdatedMachines);
    });
  }

  private defineStreams(): void {
    this.initialMachines$ = this.machineRestService.getMachines$().pipe(
      map(machineData => machineData.data)
    );

    this.machines$ = combineLatest([
      this.initialMachines$,
      this.updatedMachines$
    ]).pipe(
      map(([initialMachines, updatedMachines]) => {
        return initialMachines.map(initialMachine => {
          const foundUpdatedMachine = updatedMachines.find(updatedMachine => initialMachine.id === updatedMachine.machine_id);
          if(foundUpdatedMachine) {
            initialMachine.status = foundUpdatedMachine.status;
          }
          return initialMachine;
        })
      })
    );
  }

  public getStatusColor(machineStatus: MachineStatus): string {
    return MachineStatusColor[machineStatus];
  }
}
