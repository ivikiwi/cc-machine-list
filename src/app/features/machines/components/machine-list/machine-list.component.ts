import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Machine } from '../../interfaces/machine';
import { MachineService } from '../../services/machine.service';

@Component({
  selector: 'app-machine-list',
  templateUrl: './machine-list.component.html',
  styleUrls: ['./machine-list.component.scss']
})
export class MachineListComponent implements OnInit {
  public machines$: Observable<Machine[]>;

  constructor(private machineService: MachineService) { }

  ngOnInit(): void {
    this.machines$ = this.machineService.machines$;
  }

}
