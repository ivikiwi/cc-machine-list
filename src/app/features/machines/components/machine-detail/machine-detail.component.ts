import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MachineStatusColor } from '../../enums/machine-status-color';
import { Machine } from '../../interfaces/machine';
import { MachineEvent } from '../../interfaces/machine-event';
import { MachineService } from '../../services/machine.service';

@Component({
  selector: 'app-machine-detail',
  templateUrl: './machine-detail.component.html',
  styleUrls: ['./machine-detail.component.scss']
})
export class MachineDetailComponent implements OnInit {
  @Input() machine: Machine;

  public machineEvents$: Observable<MachineEvent[] | undefined>;
  public isMachineEventsVisible: boolean = false;

  constructor(private machineService: MachineService) { }

  ngOnInit(): void {
  }

  public toggleLastEvents(): void {
    if(this.isMachineEventsVisible) {
      this.isMachineEventsVisible = false;
      return;
    }
    this.machineEvents$ = this.machineService.getLastMachineEvents$(this.machine.id);
    this.isMachineEventsVisible = true;
  }

  public getStatusColor(): string {
    return this.machineService.getStatusColor(this.machine.status);
  }
}
