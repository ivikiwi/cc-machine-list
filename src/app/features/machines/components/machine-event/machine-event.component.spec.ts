import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MachineRestService } from '../../services/machine-rest.service';

import { MachineEventComponent } from './machine-event.component';

describe('MachineEventComponent', () => {
  let component: MachineEventComponent;
  let fixture: ComponentFixture<MachineEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MachineEventComponent ],
      imports: [HttpClientTestingModule, HttpClientModule], 
      providers: [MachineRestService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
