import { Component, Input, OnInit } from '@angular/core';
import { MachineEvent } from '../../interfaces/machine-event';
import { MachineService } from '../../services/machine.service';

@Component({
  selector: 'app-machine-event',
  templateUrl: './machine-event.component.html',
  styleUrls: ['./machine-event.component.scss']
})
export class MachineEventComponent {
  @Input() machineEvent: MachineEvent;

  constructor(private machineService: MachineService) {}

  public getStatusColor(): string {
    return this.machineService.getStatusColor(this.machineEvent.status);
  }
}
