import { Machine } from "./machine";

export interface MachineData {
    data: Machine;
}