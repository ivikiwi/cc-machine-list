import { Machine } from "./machine";

export interface MachinesData {
    data: Machine[];
}