import { MachineStatus } from "../types/machine-status-type";
import { MachineEvent } from "./machine-event";

export interface Machine {
    id: string;
    status: MachineStatus;
    machine_type: string;
    longitude: number;
    latitude: number;
    last_maintenance: string;
    install_date: string;
    floor: number;
    events?: MachineEvent[];
}