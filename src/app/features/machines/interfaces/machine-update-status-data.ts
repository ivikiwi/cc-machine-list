import { MachineStatus } from "../types/machine-status-type";

export interface MachineUpdateStatusData {
    machine_id: string;
    id: string;
    timestamp: string;
    status: MachineStatus;
}