import { MachineStatus } from "../types/machine-status-type";

export interface MachineEvent {
    timestamp: string;
    status: MachineStatus;
}