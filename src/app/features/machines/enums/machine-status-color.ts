export enum MachineStatusColor {
    running = 'blue',
    idle = 'orange',
    finished = 'green',
    errored = 'red',
    repaired = 'grey'
}