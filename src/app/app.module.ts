import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MachineListComponent } from './features/machines/components/machine-list/machine-list.component';
import { MachineDetailComponent } from './features/machines/components/machine-detail/machine-detail.component';
import { MachineEventComponent } from './features/machines/components/machine-event/machine-event.component';

@NgModule({
  declarations: [
    AppComponent,
    MachineListComponent,
    MachineDetailComponent,
    MachineEventComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
